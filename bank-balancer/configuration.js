/* Configuration Settings */

var loginAccountNumber    = "XXXXXXXXX"; /* Account number used to login. Only numbers allowed       */
var loginCardNumber       = "XXX";       /* Card Number used to login ie. 123                        */
var loginPinCode          = "XXXXX";     /* PIN Code used to login, sadly in plain-text              */
var primaryAccountNumber  = "XXXXXXXXX"; /* Only numbers allowed ie. 123456782                       */
var depositAccountNumber  = "XXXXXXXXX"; /* Only numbers allowed ie. 123456782                       */
var mustHaveAmount        = "50.00";     /* Amount that must always be on primary account. ie. 50.00 */

var enableScreenshots          = false; /* Enable screenshots at each step, stored in /images  */
var enableMacNotification      = false; /* Enable to use Mac OS X Notification Center          */
var enablePushoverNotification = false; /* Enable to send push notifications with Pushover.net */
var pushoverToken              = "";
var pushoverUser               = "";
var pushoverDevice             = "";

/* End Configuration Settings */

/* Do not edit below this lines */
var dirty = false
if (!loginAccountNumber.match(/\d{7,9}/)) {
  console.log("Invalid loginAccountNumber. Check configuration.js");
  dirty = true;
}

if (!loginCardNumber.match(/\d{3}/)) {
  console.log("Invalid loginCardNumber. Check configuration.js");
  dirty = true;
}

if (!loginPinCode.match(/\d{5}/)) {
  console.log("Invalid loginPinCode. Check configuration.js");
  dirty = true;
}

if (!primaryAccountNumber.match(/\d{7,9}/)) {
  console.log("Invalid primaryAccountNumber. Check configuration.js");
  dirty = true;
}

if (!depositAccountNumber.match(/\d{7,9}/)) {
  console.log("Invalid depositAccountNumber. Check configuration.js");
  dirty = true;
}

if (!parseFloat(mustHaveAmount)) {
  console.log("Invalid mustHaveAmount. Check configuration.js");
  dirty = true;
}

if (!typeof enableScreenshots == 'boolean') {
  console.log("Invalid enableScreenshots. Check configuration.js");
  dirty = true;
}

if (!typeof enableMacNotification == 'boolean') {
  console.log("Invalid enableMacNotification. Check configuration.js");
  dirty = true;
}

if (!typeof enablePushoverNotification == 'boolean') {
  console.log("Invalid enablePushoverNotification. Check configuration.js");
  dirty = true;
}

if (!dirty) {
  exports.loginAccountNumber    = loginAccountNumber;
  exports.loginCardNumber       = loginCardNumber;
  exports.loginPinCode          = loginPinCode;
  exports.primaryAccountNumber  = primaryAccountNumber;
  exports.depositAccountNumber  = depositAccountNumber;
  exports.mustHaveAmount        = mustHaveAmount;

  exports.enableScreenshots          = enableScreenshots;
  exports.enableMacNotification      = enableMacNotification;
  exports.enablePushoverNotification = enablePushoverNotification;
  exports.pushoverToken              = pushoverToken;
  exports.pushoverUser               = pushoverUser;
  exports.pushoverDevice             = pushoverDevice;
  exports.failed                     = false;
} else {
  exports.failed                     = true;
}
