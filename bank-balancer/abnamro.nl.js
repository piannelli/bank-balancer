/* For configuration, please see the configuration.js file */

config = require("./configuration.js");

if (config.failed) {
  phantom.exit();
}

var startUrl = "https://www.abnamro.nl/portalserver/nl/prive/index.html?l";

spawn = require("child_process").spawn;
var page = new WebPage(), currentStep = 0, loadInProgress = false;

page.injectJs('includes/jquery.min.js');
page.settings.userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.6 Safari/537.11";

Date.prototype.today = function(){ 
    return ((this.getDate() < 10)?"0":"") + this.getDate() +"/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +"/"+ this.getFullYear() 
};

Date.prototype.timeNow = function(){
     return ((this.getHours() < 10)?"0":"") + this.getHours() +":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
};

function log(msg, printTimestamp) {
    var date = new Date();
    printTimestamp = typeof printTimestamp !== 'undefined' ? printTimestamp : true;
    if (typeof msg === "string") {
        console.log((printTimestamp ? "[" + date.today() + " " + date.timeNow() + "] " : "") + msg);
    }
}

page.onError = function(msg, trace) {
  // Do nothing, if there are errors on page is not our business ;)
};

page.onConsoleMessage = function(msg) {
  // Do nothing, we don't want messages coming from pages
};

page.onLoadStarted = function() {
  loadInProgress = true;
};

page.onLoadFinished = function() {
  loadInProgress = false;
};

var steps = [
  function() {
    log("Loading login page");
    page.open(startUrl);
  },
  function() {
    log("Entering credentials");
    page.evaluate(function(loginAccountNumber, loginCardNumber, loginPinCode) {
      $("input[name=accountnumberInput]").val(loginAccountNumber);
      $("input[name=cardnumberInput]").val(loginCardNumber);
      $("input[name=login-pincode]").val(loginPinCode);
      $("input[data-tl=Generic-Login]").click();
    }, config.loginAccountNumber, config.loginCardNumber, config.loginPinCode);
  },
  function() {
    log("Checking accounts");
    results = page.evaluate(function(primaryAccountNumber, depositAccountNumber, mustHaveAmount) {
      var amountTxt = $("li[data-aid=accountItem][data-account=" + primaryAccountNumber + "] .mcf-balance").text();
      var amount    = parseFloat(amountTxt.replace("€", "").replace(/ /g, "").replace(".","").replace(",",".")); // "€ 100,00" => 100.00

      if (amount < mustHaveAmount) {
        notificationMessage      = "The amount on account " + primaryAccountNumber + " is lower than €" + mustHaveAmount;
        notificationTitle        = "Account " + primaryAccountNumber + " below €" + mustHaveAmount;
        amountToTransfer         = parseFloat(mustHaveAmount) - amount;
        senderAccountNumber      = depositAccountNumber;
        beneficiaryAccountNumber = primaryAccountNumber; 
      } else if (amount > mustHaveAmount) {
        notificationMessage      = "The amount on account " + primaryAccountNumber + " is higher than €" + mustHaveAmount;
        notificationTitle        = "Account " + primaryAccountNumber + " exceeds €" + mustHaveAmount;
        amountToTransfer         = amount - parseFloat(mustHaveAmount);
        senderAccountNumber      = primaryAccountNumber;
        beneficiaryAccountNumber = depositAccountNumber; 
      } else {
        notificationMessage      = "The amount on account " + primaryAccountNumber + " is enough. Exiting";
        return new Array(false, false, false, notificationMessage, "");
      }

      amountToTransfer = Math.floor(amountToTransfer * 100) / 100;

      $("li[data-aid=accountItem][data-account=" + senderAccountNumber + "]").click();
      return new Array(amountToTransfer, senderAccountNumber, beneficiaryAccountNumber, notificationMessage, notificationTitle);
    }, config.primaryAccountNumber, config.depositAccountNumber, config.mustHaveAmount);

    log(results[3]);
    notificationTitle = results[4];

    if (results[0] == false) {
      return logout();
    } else {
      amountToTransfer         = results[0];
      senderAccountNumber      = results[1];
      beneficiaryAccountNumber = results[2];
    }
  },
  function() {
    log("Preparing transfer of €" + amountToTransfer + " from " + senderAccountNumber + " to " + beneficiaryAccountNumber);
    action = (senderAccountNumber == config.primaryAccountNumber) ? "to" : "from";
    notificationMessage = "Transferring €" + amountToTransfer + " " + action + " deposit account.";

    if (config.enableMacNotification) {
      macosxNotification(notificationMessage, notificationTitle);
    }

    if (config.enablePushoverNotification) {
      pushoverNotification(notificationMessage, notificationTitle);
    }

    page.evaluate(function() {
        $("a[data-aid=createPaymentInstruction]").click();
    });
  },
  function() {
    log("Entering amount");
    page.evaluate(function(amountToTransfer) {
      var amount = (amountToTransfer + "").split(".")[0] || 0;
      var decimals = (amountToTransfer + "").split(".")[1] || 0;
      $("input[name=amountTotal]").val(amountToTransfer);
      $(".mcf-select-beneficiary-button").click();
    }, amountToTransfer);
  },
  function() {
    log("Selecting beneficiary " + beneficiaryAccountNumber);
    page.evaluate(function(beneficiaryAccountNumber) {
      $(".mcf-widget-addressbook li[data-accountnumber=" + beneficiaryAccountNumber + "]").click();
    }, beneficiaryAccountNumber);
  },
  function() {
    log("Sending transfer order");
    page.evaluate(function() {
      $("input[data-aid=submitMutationToSummary]").click();
    });
  },
  function() {
    log("Confirming transaction");
    page.evaluate(function() {
      $("input[data-tl=Generic-Verzenden]").click();
    });
  },
  function() {
    log("Sending PIN code");
    page.evaluate(function(loginPinCode) {
      $("input[name=login-pincode]").val(loginPinCode);
      $("input[data-aid=submitSimpleSignSofttoken]").click();      
      log("Transfer Completed");
    }, config.loginPinCode);
  },
  function() {
    log("Logging out");
    page.evaluate(function() {
      jQuery('a[data-tl=Generic-LogUit]').click();
    });
  },
  function() {
    log("Logged out");
  }
]

function macosxNotification(message, title) {
  var child = spawn("./bin/terminal-notifier.app/Contents/MacOS/terminal-notifier", ["-message", message, "-title", "Bank account alignment", "-subtitle", title]);

  child.on("exit", function (code) {
    log("Mac OS X notification status: " + ((code) ? "failed" : "success"));
  })
}

function pushoverNotification(message, title) {
  var data = {
      "token"   : config.pushoverToken,
      "user"    : config.pushoverUser,
      "device"  : config.pushoverDevice,
      "title"   : title,
      "message" : message.replace("€", "\u20AC")
  };

  data = Object.keys(data).map(function(k) {
    return encodeURIComponent(k) + "=" + encodeURIComponent(data[k])
  }).join("&");

  var req = new WebPage();
  req.open("https://api.pushover.net/1/messages.json", "POST", data, function(status) {
    log("Pushover notification status: " + status);
  });
}

function logout() {
  currentStep = (steps.length - 1) - 2; // logout steps are last 2
}

function quit() {
  setInterval(function() {
    log("Finished");
    phantom.exit();
  }, 5000); // Allow any pending request to finish gracefully 
}

interval = setInterval(function() {
  if (!loadInProgress && typeof steps[currentStep] == "function") {
    steps[currentStep]();
    if (config.enableScreenshots) {
        page.render("screenshots/step" + (currentStep + 1) + ".jpg", { quality: 50 });
    }
    currentStep++;
  }

  if (typeof steps[currentStep] != "function") {
    return quit();
  }
}, 13000);


log("********************************************************************", false);
log("***   THIS SOFTWARE IS INTENDED FOR EDUCATIONAL PURPOSES ONLY    ***", false);
log("********************************************************************", false);
log("***    /!\\ This program is acting on your Bank Account  /!\\      ***", false);
log("***  and is provided 'AS IS', complying to BSD License. Use this ***", false);
log("***  software at your own risk and ONLY if you fully understand  ***", false);
log("***  what is doing! Your bank may suspend your access if in      ***", false);
log("***  violation of their Terms of Use for online banking. Author  ***", false);
log("***  declines any responsability from using this software.       ***", false);
log("********************************************************************", false);
log("", false);
log("==========>   Press CTRL+C (or Command+C) to quit now   <============", false);
log("", false);

setTimeout(function() { log("Program started"); }, 5000);
