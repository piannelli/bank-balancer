# Bank Balancer

Purpose of this software is to login to your Dutch bank account on ABN Amro and ensure that the amount available on your Primary Account is whatever you have chosen it to be. Should the amount exceed your fixed amount, the exceeding amount will be transferred to your Deposit Account. On the other hand, should your amount be in deficit or below the amount you have chosen, funds will be automatically transferred from your Deposit Account to the Primary Account.

## Important Notes
 This tool is released for educational purposes and shows how automation can be applied to everyday's life.  
When using it, please bear in mind that __THIS TOOL OPERATES ON YOUR BANK ACCOUNT__ and that your personal PIN Code is stored __IN PLAIN TEXT__ in the configuration file.  
This means that lot of things can go wrong.  
For instance, anyone opening the configuration file can get your login details and operate on your bank account. This is a big security risk and because of this you should consider whether to use this software.  
Moreover, if you run this tool very often you may break the Terms of Use of your online banking account and the bank may suspend it.  
__IN A FEW WORDS__: consider this package as a bare example of what you can do to automate your life with simple tools like [PhantomJS](http://www.phantomjs.org), [Pushover](http://www.pushover.net) and [Terminal Notifier](https://github.com/alloy/terminal-notifier).  
__Be nice to the banks, they still keep your money safe :)__

## How it works
This software use PhantomJS to simulate an headless browser and it navigates through pages just like a human being would do (clicking stuffs and typing inputs).  
To make it even more interesting, when a meaningful action is executed in your bank account, it will send a notification to your Notification Center (if your run it on Mac OS X 10.8) and/or using your Pushover account, meaning that you may receive the notification on your Mac and/or in all your mobile devices, Android and iOS based.

## Initial Configuration
Inside bank-balancer you will find a file called __configuration.js__ that needs some parameters :

var loginAccountNumber    = "XXXXXXXXX"; <= Account used to login. Only numbers allowed  
var loginCardNumber       = "XXX";       <= Card Number used to login ie. 123                       
var loginPinCode          = "XXXXX";     <= PIN Code used to login, __sadly in plain-text__  
var primaryAccountNumber  = "XXXXXXXXX"; <= Only numbers allowed ie. 123456782  
var depositAccountNumber  = "XXXXXXXXX"; <= Only numbers allowed ie. 123456782  
var mustHaveAmount        = "50.00";     <= Amount that must always be on primary account.

var enableScreenshots          = false; <= Enable screenshots at each step, stored in /screenshots  
var enableMacNotification      = false; <= Enable to use Mac OS X 10.8 Notification Center  
var enablePushoverNotification = false; <= Enable to send push notifications with Pushover.net  
var pushoverToken              = ""; <= Token of the App in Pushover.net  
var pushoverUser               = "";  <= Your user in Pushover.net  
var pushoverDevice             = ""; <= The device to notify. Leave empty for all  

## Launching Bank Balancer
Based on your operating system, you have to launch the correct file :

1. Windows users, double click on __start_windows__
2. Mac OS X users, double click on __start_mac.command__
3. Linux users, use __sh start\_linux\_x86.sh__ or __sh start\_linux\_x86\_64.sh__ accordingly

## Screenshots
###Normal execution, primary account had less than programmed 100 Euro
![Normal Run](https://bitbucket.org/piannelli/bank-balancer/raw/master/includes/assets/normal_run.png "Normal run")
###Account had already the programmed 100 Euro amount
![Enough Run](https://bitbucket.org/piannelli/bank-balancer/raw/master/includes/assets/enough_run.png "Enough run")
###Notification in Mac OS X 10.8
![Mac Notification](https://bitbucket.org/piannelli/bank-balancer/raw/master/includes/assets/mac_notification.png "Normal run")
###Pushover notification on Android
![Pushover](https://bitbucket.org/piannelli/bank-balancer/raw/master/includes/assets/pushover.png "Pushover")

# License
Copyright (c) 2013 Paolo Iannelli <info@paoloiannelli.com>

This project is distributed under BSD License.  
For license's terms please refer to LICENSE.BSD.
This project makes use of other BSD and MIT licensed software.  
Relative licenses can be found in the "bin" folder.
